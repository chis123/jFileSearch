package net.qqxh.resolve2text.impl;

import net.qqxh.resolve2text.File2TextResolve;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Component
public class Doc2TextResolve implements File2TextResolve {
    private static String TYPE = "doc";

    @Override
    public String resolve(byte[] data) {
        String text = "";
        InputStream fis = null;
        WordExtractor ex = null;
        try {
            fis = new ByteArrayInputStream(data);
            ex = new WordExtractor(fis);
            text = ex.getText();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ex != null) {
                try {
                    ex.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return text;
    }

    @Override
    public String getType() {
        return TYPE;
    }

}
