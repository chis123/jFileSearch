package net.qqxh.search;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.net.InetAddress;

import java.net.UnknownHostException;

@Configuration
public class EsConfig {
    @Value("${es_node_address}")
    private String esNodeAddress;
    @Value("${es_node_port}")
    private int esNodePort;

    @Bean(name="esclient")
    public TransportClient esclient() throws UnknownHostException {

        InetSocketTransportAddress node = new InetSocketTransportAddress(InetAddress.getByName(esNodeAddress), esNodePort);
        /*设置集群名称*/
        Settings settings = Settings.builder().put("cluster.name", "elasticsearch").build();
        TransportClient transportClient = new PreBuiltTransportClient(settings);
        transportClient.addTransportAddress(node);
        TransportClient client = new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddress(node);

        return client;
    }

}
