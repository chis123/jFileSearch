package net.qqxh.controller.apiUtil;

public class BaseJson {
    private String status;//请求状态
    private String msg;//数据错误  \ 成功等标题
    private String detail;//用于描述详细信息：比如：手机号不能为空 -没有信息时不会返回此字段
    private Object Data;//业务数据

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }
}
