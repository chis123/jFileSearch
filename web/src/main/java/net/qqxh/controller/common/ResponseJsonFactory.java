package net.qqxh.controller.common;

import org.springframework.stereotype.Component;

@Component
public class ResponseJsonFactory {

    public CommonJson getSuccessJson(String detaile, Object data) {
        CommonJson baseJson = new CommonJson();
        baseJson.setStatus("200");
        baseJson.setMsg("成功");
        baseJson.setDetail(detaile);
        baseJson.setData(data);
        return baseJson;
    }

    public CommonJson getErrorJson(String detaile, Object data) {
        CommonJson baseJson = new CommonJson();
        baseJson.setStatus("500");
        baseJson.setMsg("失败");
        baseJson.setDetail(detaile);
        baseJson.setData(data);
        return baseJson;
    }

}
