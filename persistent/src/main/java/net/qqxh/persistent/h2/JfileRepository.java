package net.qqxh.persistent.h2;

import net.qqxh.persistent.Jfile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public interface JfileRepository extends CrudRepository<Jfile,String> {
}
